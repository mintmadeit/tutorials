# Discord Coding Academy

This is the beginnings of moving the tutorials that are currently stored in discord to a website for easier readability and to also reduce the amount of channels.

# Contributing


Anyone can submit a pull request to modify an existing tutorial or to submit an entirely new tutorial.


This collection of tutorials is written using [Markdown](https://www.markdownguide.org/)
and then compiled using [mdBook](https://github.com/rust-lang-nursery/mdBook)
you can download it from their [releases](https://github.com/rust-lang-nursery/mdBook/releases)
page.

Main part of mdBook that you will be using for writing tutorials is
```
mdbook serve
```
It will start a webserver at http://localhost:3000/ which you can visit using your browser to see the rendered website. As you edit you can save the files and the webpage will auto reload showing changes instantly.

## Important stuff included in this repository are:
- `.gitlab-ci.yml`, set up to automatically build the site and release it on GitLab Pages
- `book.toml`, just some basic configuration settings for mdBook
- `.gitignore`, stops the output from mdBook being committed to the Git repository
- the `theme` folder, contains files to override mdBook's default themes, templates etc, currently have a `highlight.js` file there because mdBooks default `highlight.js` is missing a bunch of languages.

## The `src` directory
This is where the actual Markdown files and also images for the tutorials go.
`SUMMARY.md` is the indexing file; it specifies all the Markdown files that are part of the tutorial. When everything is compiled into a website this file is read to decide what order to put the pages in. Also inside the src directory you will find a folder for each tutorial. For the Docker tutorial I just got a `.md` file for each tutorial then an imgs folder with all the images I use in that tutorial. You can layout the files in your tutorial however you want though. Just got to make sure all the links are done correctly.

## Licensing
All files in this repository are licensed under CC0 1.0 unless a different license is stated within the first couple of lines of a file. If that happens then that license will come into effect for that file only. The CC0 license text can be found in the root of this repository named `LICENSE`.
