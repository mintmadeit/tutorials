# Useful Need-To-Knows

In this section I'll tell you about some neat tips and tricks which can make your bot making life easier and help you craft a better service.

## Customisable prefixes

Prefixes are big part of any bot and you probably have a unique one for your own, but uniqueness comes with the price of being less convenient. What if you could make that unique prefix the default option but letting each guild set its own prefix? Well, fear no more, as that is very much possible. `discord.py` actually allows you to pass other a function to the `command_prefix` parameter when creating the Bot instance.  
All you have to do is write a coroutine function that takes both a bot object and a message object as arguments and returns either a string or a collection of strings. Here's an example:
```python
# The default prefix to fallback in case the bot is activated in DMs or if a guild doesn't have any custom prefix set
# You could also put this on a config file if you use one
DEFAULT_PREFIX = "bot!"

async def get_prefix(bot, message):
    """Gets the custom prefix for a guild if applicable"""
    if message.guild:
        prefix = ...  # Get the prefix from a database, for example
        # Example of seeing if a guild doesn't have a custom prefix set
        return prefix if prefix else DEFAULT_PREFIX

    # Fallback onto the default prefix
    return DEFAULT_PREFIX
```
Then just pass it on the Bot constructor, like this: `bot = commands.Bot(command_prefix=get_prefix, ...)`.  

You then should make a command to change the prefix with an option to revert back to the default one.


## libneko

`libneko` is a library originally written by Espy (aka `nekoka.tt#4766`) and currently maintained by me which contains a lot of tools to help you with making bots using `discord.py`. Its toolset ranges from better Embed classes to a powerful paginator and navigator module. I'll be guiding you through what I consider to be the most relevant features of this package.

To install this, just do `pip install git+https://gitlab.com/Tmpod/libneko` in your venv or add `git+https://gitlab.com/Tmpod/libneko` to your requirements file.

#### Embed class

`discord.py` offers an easy way of making embed objects to use when sending a message. However, similarly to other things, it doesn't care about limits, and that's where `libneko.Embed` comes into place. It's written from scratch to include char limit checks, URL validation, more colour options (defaults to `#363940` which appears to be "invisible" with dark theme), automatic UTC timestamps and better image URL handling. You use them like you `discord.py`'s, just got to use `libneko.Embed` instead of `discord.Embed`.  
Here's a little example:
```python
embed = libneko.Embed(title="This is a title", description="And this is a description!")
embed.set_image(url=str(ctx.bot.user.avatar_url))
embed.add_field(name="I'm a field name", value="And I'm a field value!")
await ctx.send(embed=embed)
```
Would produce (with my bot)

![`libneko` Embed example](./imgs/embed-example.png)

#### `superuser` and `help` extensions

`libneko` comes with a submodule called `extras` which contains a couple of neat extensions. The first one is `superuser` which you can load by doing `bot.load_extension("libneko.extras.superuser")`. It adds one comand that actualy works like two:

* `exec` - allows you to execute arbitrary Python code directly from Discord and receive both stdout and stderr, with built-in output pagination. Both the bot and command context objects are injected into scope and everything is wrapped in a coroutine function so you can `await` stuff right on the spot.
* `shell` - allows you to run whatever you give it in a shell session, also capturing stdout and sterr and paginating the output.  

There's `panic` command which disables this module until it's reloaded again.

The second extension that comes in the `libneko.extra` submodule is the `help` extension which provides a nice paginated help command which includes basically all the features present in the default one and some original ones, while being customisable. To load it just do `bot.load_extension("libneko.extras.help")` and it will also remove the default help command.

#### The `pag` submodule

This submodule is what I believe to be the biggest attraction in `libneko`. It provides and extensive API to paginate text and run fancy navigators. It is made up of four main components:

* `paginator` - a powerful paginator that takes whatever text you have and outputs neat pages that will fit in Discord without issues. It even supports custom substitutions when paginating.
* `navigator` - the "engine" that takes pages of text and makes them scrollable and controlable through the use of Discord reactions.
* `reactionbuttons` - essencially representing reaction, these objects help you customise the behaviour of navigator buttons. They can take any action to execute when pressed.
* `factory` - a convenience utility that removes some of the boring work of creating lots of stuff to get your dream navigator working.

*You can read more on the [documentation](https://koyagami.gitlab.io/libneko/technical/pag.html).*

<!-- Extend this section to cover more of the possibilities of this module -->

<!-- Add more tips and tricks eventually -->

