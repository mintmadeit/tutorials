# Extensions

Welcome to the last main section of this tutorial! Here, I'll be teaching you how you can break down your code into smaller components so that each part of your bot can be kept clean, more manageable and loadable/unloadable on the fly.  
For this we'll use the extensions and cogs system `discord.py` provides. 

It's important to know well what are extensions and cogs:

* `extensions` - Python files that must have an entry-point function called `setup` and an optional clean-up function called `teardown` that take a single argument of Bot type which can be used to load commands or cogs into your bot.
* `cogs` - Classes derived from `commands.Cog` that hold commands and listeners. Very useful to organise stuff into smaller, isolated components.

A good way of splitting your bot's code is to combine extensions and cogs.  
Let's imagine we have some commands that provide info relating to the bot, like a description, system status and bot stats. We could pack all of them into a cog and shove it into an extension. Doing this would be really easy. Watch!

`info.py`
```python
from discord.ext import commands


class InfoCog(commands.Cog):
    """Bundle of useful informative commands about the bot"""

    @commands.command(name="about")
    async def about_command(self, ctx):
        ...

    @commands.command(name="status")
    async def system_status_command(self, ctx):
        ...

    @commands.command(name="stats")
    async def bot_stats_command(self, ctx):
        ...


def setup(bot):
    bot.add_cog(InfoCog())
```
Seems complicated? It's not :P  

Let's break this step by step.  
First, I obviously import the commands module which is where all of this functionality comes from. Then, I create a class that derives from `commands.Cog` and I put some commands in there (they have to take the `self` argument before all the others since we're inside a class and these are not static or class methods). However, as you can see, the decorators are not exacly the same as they were in the main file. That's because the bot object isn't in scope inside the class, but the framework will register all commands and listeners in a cog when you load it. Finally, I write the necessary `setup` function which takes a bot client as the only argument and I use it to load the cog.  

Adding listeners is also slightly different from what you do in the main file, because again, the bot isn't in scope. To do this you have to use the `commands.Cog.listener` decorator which behaves the same as `bot.listen`. Here's a quick example of a cog with an event listener:
```python
class ReadyBannerCog(commands.Cog):

    @commands.Cog.listener("on_ready")
    async def print_ready_banner_on_ready(self):
        # This is not properly formatted as it's just an example
        print(
            """
            ----------
            I'M READY!
            ----------
            """
        )
```

Cogs also provide some other useful special methods that allow you to add extra functionality to them, like cog-wide checks, error handlers, pre and post-invoke hooks, etc. You can find the full list of special methods [**here**](https://discordpy.readthedocs.io/en/latest/ext/commands/cogs.html#special-methods).

Lastly, you can override cog names or give default attributes to all commands in a cog using metaclass parameters, like this:
```python
class FooBarCog(commands.Cog, name="FooBar Cog", command_attrs={"hidden": True})
```

---

And that's all there is to this! We've finally reached the end of this `discord.py` tutorial where you hopefuly learnt how to build your own bot using Python! Before you go though, take a look at the [**Useful Need-to-Knows**](./need-to-knows.html) section where I gathered a bunch of tips and tricks that may prove themselves very useful in your bot making adventure!

Thanks a lot for reading this tutorial and I hope you've enjoyed it! If you have any suggestion or if you are having trouble with anything related to `discord.py`, please don't hesitate in coming to our [**Discord server**](https://discord.gg/RDKfMcC) and place your question in either **`#py-help-0`** or **`#py-help-1`**
