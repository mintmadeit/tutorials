# Token Leaked 
***by `God Empress Verin#6160`***  

![Banner](./imgs/token-leaked-title.png)

---

This tutorial focuses on helping bot developers determine if their token has been leaked and assists them in regenerating it to prevent damage. It also goes over how your token may have been leaked and steps you can take to prevent this from happening in the future.  
To be blunt, if you think your token is leaked, it probably is. There's no such thing as being too safe, and this is especially true when potentially hundreds of users and channels are at risk of being affected by your bot.  
Some signs to look out for that may point to your token being leaked:

+ Your bot is randomly kicking/banning users
+ Your bot is deleting channels or generally being very destructive
+ Your bot is spamming/being very obnoxious

If your bot exhibits any of these and you weren't the one who set it off to do these things, then there is no denying that your token has been leaked

**Q:** I don't understand, how did my token get leaked?  
**A:** Well, its entirely likely that you were't careful enough with your token. You should always treat it as you would your house or car keys. Much like you wouldn't want someone to get ahold of those, you very much don't want someone getting ahold of your token. Much like with a house or car, any damages that are caused as a result of your bot token being leaked ultimately fall on you.

Some possibilities for how it got out:

- You accidentally posted your token on a public code/file sharing site.  
- You mistakenly shared your token with someone over Discord.  
- You've been really down on your luck lately and someone breached your Discord account.  

That's enough discussing the how and the why, Lets move on to what to do next.
To begin, head over to the Discord developers portal. That can be found [**here**](https://discordapp.com/developers/applications/).

Go ahead and select your application and head to the bot tab. If you need help finding it, [**Making the Bot App**](../starting/making-the-bot.html) should be helpful to you.
You should now be looking at a page that looks like this:

![App Landpage](./imgs/app-landpage.png)

Up front in center, go ahead and click the **`Regenerate`** button

![Regenerate Button](./imgs/regen-button.png)

That's it! Its that simple! Your token has been reset! Now just click copy and go paste your new token wherever in your bot your token is supposed to go!

Do make sure that you are careful not to leak your token again! You may also consider changing your discord account password just to be safe. If your bot is on GitHub/GitLab, make sure you remove your token from your bots files before pushing changes or put the token in an external file and add it to the `.gitingore`.
