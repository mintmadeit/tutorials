# Running a Python Bot

Ok, so you have hopefully installed Docker and asserted that it works, time to actually use it for something useful.

## Files
You will need the following 3 files for this guide

`bot.py`
```python
import os

import discord
from discord.ext import commands

bot = commands.Bot(command_prefix='!')

@bot.command()
async def say(ctx, *, something):
    msg = f'{ctx.author} said: {something}'
    print(msg)
    await ctx.send(msg)

# using environment variables because what kind of dummy keeps their token in their code smh
bot.run(os.getenv('TOKEN'))
```
`requirements.txt`
```
discord.py
```
`Dockerfile` (case-sensitive and no extension)
```Dockerfile
# Dockerfiles are a list of instructions that Docker will follow to create your container for you
# The Docker style guide dictates that instructions should be capitalized but that is not enforced...
FROM alpine
# Every Docker file will normally begin with a FROM line. Saying the name of the image that you wish to use as a base image
# If you don't have this 'FROM scratch' will be assumed, which is probably not what you want
ADD . /src
# The ADD command will copy files from your computer to a specified directory in the container
# Here I'm adding '.' to /src, '.' is the current directory so I'm adding the whole contents of the current
# directory to a folder named 'src' in the container

WORKDIR /src
# This is for changing the current working directory (basically does the same thing as 'cd' would in the terminal)
RUN apk add --no-cache python3; pip3 install -r requirements.txt
# the RUN command will execute commands during the build process, you can have many run lines
# but each run line will add a new layer to the resulting image (there is a limit to the layers you probably won't 
# reach it though) but each layer can add a lot of size to the image so I use ';' to separate commands which is just standard
# terminal stuff doing this means I only end up adding one new layer
# The first part 'apk add --no-cache python3'
# will install python3.6.6 using AlpineLinux's package manager, similar to how 'apt-get install' works
# then 'pip3 install' is ran reading the contents of the requirements.txt file to install the stuff you need
CMD python3 bot.py
# the CMD line will probably be the last one in your Dockerfile, it specifies the default command to be ran when
# you do 'docker run'. It's just a simple 'python3 bot.py', which you should have used many times before
```
So you made those 3 files and put them all in the same folder? Good! Onward to the next step!

You need to open a terminal use `cd` to move to the correct folder
```
docker build .
```
run that command and prepare for lots of text

The `docker build` command builds a Docker image, it normally takes one argument, path to the context. The context is a directory that will be used for any ADD commands in the Dockerfile and also where Docker will look for the Dockerfile to be run using `.` for the current working directory. At the bottom of the build output you should have a line like this `Successfully built 991d24fbec98`. Well, the ID bit will be different for you but you will need that for the next step. Docker uses these IDs to identify images and containers on your system. You can add 'tags' (names for things) but the ID things work fine for me.

```
docker run -ite TOKEN=<token> <id>
```
Run that command substituting the `<token>` for you real token and the `<id>` for the image's ID that you got from `docker build`.
- `docker run` you should remember this from the previous page
- `-ite` these are the flags, `i` for interactive `t` for TTY, interactive and TTY are needed for you to actually get output from Python sent to your terminal `e` is for environment variable in the syntax `<variable name>=<variable value>` to add more environment variables just add another -e flag, e.g. `-e im_awesome=True`
- `TOKEN=<token>` the TOKEN environment variable that bot.py needs to work
- `<id>` the image 'name' using its ID instead in this case

![it works!](./imgs/2.png)

It works just like magic

![in the terminal too](./imgs/3.png)

And you can see it printed in the terminal too, well isn't that fancy...

But now you must keep your terminal open or else the bot dies that might be fine for some of but running in the background is so much better luckily its easy
```
docker run -itde TOKEN=<token> <id>
```
I know it's almost the same command you ran before but with an important difference; the `d` flag it runs the bot `detached`, it will just keep chugging in the background while you do other stuff.
# Logs
But how do you find out what its up too?

Good news docker keeps its own logs!

But you got to know the container ID to find this we got to introduce a new command.
```
docker ps
```
![ps](./imgs/4.png)
You should get something like that it displays a list of all currently running Docker containers on your computer.
```
docker logs c1dbb37cdac9
```
substituting in your own container ID you should now see anything that got printed.

![log output](./imgs/5.png)

But when did I say these things well `docker logs` has a handy `-t` flag which adds time stamps!

![timestamped logs](./imgs/6.png)

Ok, fun's over, time to murder the container.
```
docker stop c1dbb37cdac9
```
Can't delete running containers, `stop` will stop them (what else would it do, sing a song? I hope not...)
```
docker rm c1dbb37cdac9
```
`rm` means remove; it is permanent action so be sure about what you are doing! But the actual image built previously still exists. Now lets make sure the container was actually removed.
```
docker ps -a
```
Lists the containers on your computer the `-a` is for all so you can see stopped containers too.

![ps -a](./imgs/7.png)

Can no longer see the container I just deleted at least but where did the rest come from? Oh right I've been testing the `docker run` lots. By default, Docker will keep containers after exiting, should have used the `--rm` but too late now...

You could just do `docker rm` to get rid of them one by one or you can let Docker clean up all by its self by running.
```
docker system prune
```
![prune?](./imgs/8.png)

Type `y` then press `enter`, check `docker ps -a`. It will probably be empty now. Great, just what I wanted! While we are at it with `docker system` run `docker system info` to see some somewhat useful info about your computer.
