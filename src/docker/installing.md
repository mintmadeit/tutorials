# Installing

I'm just going to link Docker's install guides here

## Windows 10 pro

[**Click Here**](https://docs.docker.com/docker-for-windows/install/)

You will need to set it to Linux container mode because I won't be covering Windows containers.

## New Macs

[**Click Here**](https://docs.docker.com/docker-for-mac/install/)

### Don't meet requirements for the above links? then this is for you

[**Docker Toolbox**](https://docs.docker.com/toolbox/overview/#ready-to-get-started)

## Linux

If you like to blindly typing in commands,
```
wget -qO - http://get.docker.com | sh
```
(may need `sudo`)

Read the output of this install as it mentions some further steps such as adding your self to the Docker group.

There is also more manual way of adding the repository your self but running that command is easiest

## Now you should hopefully have it installed

Time to test that it actually works!  
All Docker commands should work the same no matter how you installed it or what OS you are using. 
So open up your terminal and type this 
```
docker run --rm alpine echo 'hello world'
```
What should happen when you run this command is print "hello world" and then instantly exit.

![output](./imgs/1.png)


- `docker` is the base command for all docker related commands
- `run` runs the container, feeding any output back to your terminal
- `--rm` deletes the container when its finished, great for testing so you don't end up with a bunch of useless containers
- `alpine` this is the name of the image, can be substituted for any other images you can find on [Docker Hub](https://hub.docker.com/) I prefer to stick to Alpine though because of its size
- `echo 'hello world'` anything after the name of the image is the command to be ran, I hope you guys already know what echo does but its a very simple terminal app that just prints a message
